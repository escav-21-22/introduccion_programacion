using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaNumeros : MonoBehaviour
{
    public int numero1;
    public int numero2;
    public float numero3;

    public int suma;
    public int division;
    public int resto;

    void Start()
    {
        /* Asignaci�n (=)
         * Operaci�n que se utiliza para almacenar en una variable un determinado contenido
         */

        // Almacena en la variable numero1 el valor 7
        numero1 = 7;

        // Almacena en la variable numero3 el n�mero decimal 4.56f
        numero3 = 4.56f;

        // Almacena en la variable suma la suma entre los valores 11 y 91
        suma = 11 + 91;

        // Almacena en la variable numero1 el valor que contenga la variable numero2
        numero1 = numero2;

        // Almacena en la variable suma la suma de los valores contenidos en las variables numero1 y numero2
        suma = numero1 + numero2;
        
        // Almacena en la variable division la divisi�n entre las variables numero1 y numero2
        division = numero1 / numero2;

        // Almacena en la variable resto el resto de la divisi�n de numero1 entre numero2
        resto = numero1 % numero2;
    }
}