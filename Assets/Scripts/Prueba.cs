// Listado de bibliotecas/librer�as que se van a utilizar en el script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Nombre de la clase: Prueba (debe corresponder con el nombre del fichero)
 * Tipo de la clase: MonoBehaviour
 */
public class Prueba : MonoBehaviour
{
    /* Visibilidad de las variables:
     * public (p�blica) - La variable se puede consultar y modificar desde fuera del script. Por ejemplo, desde el editor de Unity.
     * private (privada) - La variable solo se puede consultar y modificar desde el propio script.
     */

    /* Tipo de las variables:
     * int - N�mero entero
     * float - N�mero decimal (punto flotante)
     * char - Caracter
     * string - Cadena de caracteres
     * bool - Booleano, que puede almacenar true (verdadero) o false (falso)
     */

    // Variable p�blica de tipo entero y con nombre numeroEnteroPublica
    public int numeroEnteroPublica;
    // Variable p�blica de tipo float y con nombre numeroDecimal
    public float numeroDecimal;
    // Variable p�blica de tipo string y con nombre cadenaDeCaracteres
    public string cadenaDeCaracteres;
    // Variable p�blica de tipo caracter y con nombre caracter
    public char caracter;
    // Variable p�blica de tipo booleano y con nombre booleano
    public bool booleano;

    // Variable privada de tipo entero y con nombre numeroEnteroPrivada
    private int numeroEnteroPrivada;

    // Funci�n Start, que se ejecuta �nicamente al empezar el primer frame que el objeto est� activo
    void Start()
    {
        // Imprime por consola un mensaje de prueba
        Debug.Log("Mensaje de prueba");

        // Imprime por consola un mensaje seguido del contenido de la variable numeroEnteroPublica
        Debug.Log("La variable numeroEnteroPublica contiene el n�mero " + numeroEnteroPublica);

        // Imprime por consola un mensaje, pero en dos l�neas diferentes (usando '\n'
        Debug.Log("Mensaje en l�nea 1" + '\n' + "Mensaje en l�nea 2");

        // Imprime por consola el contenido de todas las variables del script
        Debug.Log(numeroEnteroPublica);
        Debug.LogError(numeroDecimal);
        Debug.LogWarning(cadenaDeCaracteres);
        Debug.Log(caracter);
        Debug.Log(booleano);
    }

    // Funci�n Update, que se ejecuta durante todos los frames que el objeto est� activo
    void Update()
    {
        //Debug.Log("Este mensaje se repetir�a en cada frame si estuviera descomentado");
    }
}
