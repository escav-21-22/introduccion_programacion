using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropiedadesYComponentes : MonoBehaviour
{
    // Variable privada de tipo BopxCollider
    private BoxCollider boxCollider;

    public int health;

    // Start is called before the first frame update
    void Start()
    {
        // Obtiene el componente BoxCollider que tenga asignado el GameObject con el script y lo almacena en la variable boxCollider
        boxCollider = GetComponent<BoxCollider>();
        
        // Desactiva el BoxCollider almacenado en la variable boxCollider
        boxCollider.enabled = false;

        // Activa el BoxCollider que tenga asignado el GameObject 
        GetComponent<BoxCollider>().enabled = true;

        // Imprime por consola el nombre del GameObject y su etiqueta/tag
        Debug.Log("Soy el objeto " + name + " y mi tag es " + tag);

        if (GetComponent<SphereCollider>() == null)
        {
            Debug.Log("No tengo SphereCollider");
        }
    }

    // Funci�n OnCollisionEnter, que se ejecuta justo en el frame en que un objeto colisiona con el GameObject que tenga el script asignado
    // Nota: Para que funcione correctamente, es necesario que ambos GameObject tengan Collider asignado, as� como que uno de ellos tenga Rigidbody
    private void OnCollisionEnter(Collision collision)
    {
        // Imprime por consola el nombre del objeto con el que se ha colisionado
        Debug.Log(collision.gameObject.name);

        if (collision.gameObject.CompareTag("Enemy"))
        {
            health = health - 1;
            if (health == 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        Debug.Log("Estoy en contacto con " + collision.gameObject.name);
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Dejo de estar en contacto con " + collision.gameObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entra en el trigger " + other.gameObject.name);
    }
}
